class BinaryTreeNode:
    def __init__(self, val):
        self.left = None
        self.right = None
        self.val = val

    def __str__(self, depth=0):
        ret = ""

        # Print right branch
        if self.right is not None:
            ret += self.right.__str__(depth + 1)

        # Print own value
        ret += "\n" + ("    " * depth) + str(self.val)

        # Print left branch
        if self.left is not None:
            ret += self.left.__str__(depth + 1)

        return ret

    def __repr__(self):
        return '<BinaryTreeNode representation>'


class BinaryTree:
    def __init__(self):
        self.root = None

    def get_root(self):
        return self.root

    def add(self, val):
        if self.root is None:
            self.root = BinaryTreeNode(val)
        else:
            self._add(val, self.root)

    def _add(self, val, node):
        if val < node.val:
            if node.left is not None:
                self._add(val, node.left)
            else:
                node.left = BinaryTreeNode(val)
        else:
            if node.right is not None:
                self._add(val, node.right)
            else:
                node.right = BinaryTreeNode(val)

    def find(self, val):
        if self.root is not None:
            return self._find(val, self.root)
        else:
            return None

    def _find(self, val, node):
        if val == node.val:
            return node
        elif val < node.val and node.left is not None:
            self._find(val, node.left)
        elif val > node.val and node.right is not None:
            self._find(val, node.right)

    def delete_tree(self):
        self.root = None

    def __str__(self):
        return self.root.__str__()
