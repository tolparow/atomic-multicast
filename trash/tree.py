#!/usr/bin/python

class TreeNode:
    ANGLED_SYMBOL = unichr(9562)
    ANGLED_MERGED_SYMBOL = unichr(9568)
    ANGLED_T_SYMBOL = unichr(9574)
    VERTICAL_SYMBOL = unichr(9553)
    HORIZONTAL_SYMBOL = unichr(9552)

    OFFSET = 5

    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None
        self.height = 1

    def __str__(self):
        rows, vertical_lines = [], []
        self._print_node(rows=rows)

        for i in range(len(rows))[::-1]:
            v_lines = list(vertical_lines)
            for v_i in vertical_lines:
                if v_i < len(rows[i]):
                    if rows[i][v_i] == u' ':
                        rows[i][v_i] = TreeNode.VERTICAL_SYMBOL
                    else:
                        v_lines.remove(v_i)
            vertical_lines = v_lines

            new_branch = rows[i].index(TreeNode.ANGLED_SYMBOL)
            if i < len(rows) - 1 and \
                    len(rows[i + 1]) > new_branch and \
                    rows[i + 1][new_branch] in [TreeNode.ANGLED_SYMBOL, TreeNode.VERTICAL_SYMBOL]:
                rows[i][new_branch] = TreeNode.ANGLED_MERGED_SYMBOL

            if i > 0 and rows[i - 1][new_branch] == TreeNode.HORIZONTAL_SYMBOL:
                rows[i - 1][new_branch] = TreeNode.ANGLED_T_SYMBOL

            vertical_lines.append(new_branch)

        return u'\n'.join([u''.join(row) for row in rows]).encode('utf-8')

    def _print_node(self, level=0, rows=[]):
        rows.append(
            list(
                u' ' * (level * TreeNode.OFFSET) +
                TreeNode.ANGLED_SYMBOL +
                TreeNode.HORIZONTAL_SYMBOL * TreeNode.OFFSET +
                u' ' + repr(self.val)
            )
        )

        if self.left is not None:
            self.left._print_node(level + 1, rows)

        if self.right is not None:
            self.right._print_node(level + 1, rows)


class AVLTree:

    def __init__(self):
        self.root = None

    def insert(self, key):
        self.root = self._insert(self.root, key)

    # Recursive function to insert key in
    # subtree rooted with node and returns
    # new root of subtree.
    def _insert(self, root, key):

        # Step 1 - Perform normal BST
        if not root:
            return TreeNode(key)
        elif key < root.val:
            root.left = self._insert(root.left, key)
        else:
            root.right = self._insert(root.right, key)

        # Step 2 - Update the height of the
        # ancestor node
        root.height = 1 + max(self.get_height(root.left),
                              self.get_height(root.right))

        # Step 3 - Get the balance factor
        balance = self.get_balance(root)

        # Step 4 - If the node is unbalanced,
        # then try out the 4 cases
        # Case 1 - Left Left
        if balance > 1 and key < root.left.val:
            return self.rotate_right(root)

        # Case 2 - Right Right
        if balance < -1 and key > root.right.val:
            return self.rotate_left(root)

        # Case 3 - Left Right
        if balance > 1 and key > root.left.val:
            root.left = self.rotate_left(root.left)
            return self.rotate_right(root)

        # Case 4 - Right Left
        if balance < -1 and key < root.right.val:
            root.right = self.rotate_right(root.right)
            return self.rotate_left(root)

        return root

    def rotate_left(self, z):

        y = z.right
        T2 = y.left

        # Perform rotation
        y.left = z
        z.right = T2

        # Update heights
        z.height = 1 + max(self.get_height(z.left),
                           self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left),
                           self.get_height(y.right))

        # Return the new root
        return y

    def rotate_right(self, z):

        y = z.left
        T3 = y.right

        # Perform rotation
        y.right = z
        z.left = T3

        # Update heights
        z.height = 1 + max(self.get_height(z.left),
                           self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left),
                           self.get_height(y.right))

        # Return the new root
        return y

    def get_height(self, root):
        return 0 if root is None else root.height

    def get_balance(self, root):
        if not root:
            return 0

        return self.get_height(root.left) - self.get_height(root.right)

    def get_links_csv(self):
        x = []
        self._get_links_csv(self.root, x)
        return ''.join(x)

    def _get_links_csv(self, root, s=[]):

        if root is None:
            return

        _s = ''
        if root.left is not None:
            _s += '{},{}\n'.format(root.val, root.left.val)

        if root.right is not None:
            _s += '{},{}\n'.format(root.val, root.right.val)

        s.append(_s)

        self._get_links_csv(root.left, s)
        self._get_links_csv(root.right, s)

    def __str__(self):
        return str(self.root)


if __name__ == '__main__':
    my_tree = AVLTree()

    for i in range(100):
        my_tree.insert('h' + str(i))

    print(my_tree)

    print(my_tree.get_links_csv())
