#!/usr/bin/python3
import sys
from time import sleep

from host.atomic_broadcast import ABNode
from utils.trees import Trees
from host.message import  Message


def echo(request: bytes) -> bytes:
    print(request, flush=True)
    return request


if __name__ == '__main__':
    host = sys.argv[1]

    trees = Trees.read_matrix('trees.json')
    neighbors = list(trees.keys())
    print(neighbors, host)
    neighbors.remove(host)

    server = ABNode(host)

    sleep(3)  # Wait for all to initialize

    #TODO: Send protocol messages here

    #server.send_random_messages()
