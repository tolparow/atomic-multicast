import json
from random import shuffle

from utils.n_tree import Tree


class Trees:
    def __init__(self, node_values: list, n=2):
        # matrix_dict['message_sender']['current_host'] == [children to which send message further]
        self.matrix_dict = dict()

        self.n = n

        self.node_values = node_values
        self.trees = []

        self._create_trees()

        self._combine_trees()

    def _create_trees(self):
        for node_value in self.node_values:
            self._create_tree(node_value)

    def _create_tree(self, root_node_value):
        tree = Tree(root_node_value)

        node_values = list(self.node_values)
        node_values.remove(root_node_value)
        shuffle(node_values)

        tree.fill(node_values, self.n)

        self.trees.append(tree)

    def _combine_trees(self):
        for tree in self.trees:
            self._build_matrix_row(tree)

    def _build_matrix_row(self, tree: Tree):
        nodes_children = tree.get_all_nodes_children()

        self.matrix_dict[tree.root.value] = nodes_children

    def save_matrix(self, file_name: str = 'trees.json'):
        with open(file_name, 'w') as file:
            file.write(json.dumps(self.matrix_dict))

    @staticmethod
    def read_matrix(file_name: str = 'trees.json') -> dict:
        with open(file_name, 'r') as file:
            return json.loads(file.read())


if __name__ == '__main__':
    hosts = ['127.0.0.{}'.format(i) for i in range(1, 5)]

    trees = Trees(hosts, 2)

    trees.save_matrix()