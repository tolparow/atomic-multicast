"""
This module contains scripts to provide a support of both python 2 and 3.
"""
import sys


def unicode_char(ind):
    return chr(ind) if sys.version_info >= (3,) else unichr(ind)


def unicode_str(s):
    return str(s) if sys.version_info >= (3,) else s.encode("utf-8")
