from utils.printable_tree import TreePrintableMixin, TreeNodePrintableMixin


class TreeNode(TreeNodePrintableMixin):
    def __init__(self, value, parent_node: 'TreeNode' or None):
        self.value = value
        self.parent_node = parent_node
        self.height = self.parent_node.height + 1 if self.parent_node else 0

        self.children = []

    def add_child(self, value) -> 'TreeNode':
        child = TreeNode(value, self)
        self.children.append(child)
        return child


class Tree(TreePrintableMixin):

    def __init__(self, root_value):
        self.root = TreeNode(root_value, None)

    def get_all_nodes_children(self):
        return self._get_all_nodes_children(self.root)

    def _get_all_nodes_children(self, root: TreeNode):
        res = dict()
        if root:
            res[root.value] = [child.value for child in root.children]
            for child in root.children:
                res.update(self._get_all_nodes_children(child))
        return res

    def _create_children(self, node: TreeNode, values: list):
        if not len(values):
            return

        children_values = values.pop()

        for value in children_values:
            node.add_child(value)

        for child in node.children:
            self._create_children(child, values)

    def _create_level(self, root: TreeNode, values: list, level: int):
        if root is None or len(values) == 0:
            return
        if level == 1:
            children_values = values.pop(0)
            for child_value in children_values:
                root.add_child(child_value)
        elif level:
            for child in root.children:
                self._create_level(child, values, level - 1)

    def fill(self, values: list, n_children: int):
        children_values_chunks = [values[i:i + n_children] for i in range(0, len(values), n_children)]

        queue = [self.root]

        for children_values in children_values_chunks:
            root = queue.pop(0)
            for child_value in children_values:
                queue.append(root.add_child(child_value))


if __name__ == '__main__':
    tree = Tree(0)

    values = list(range(100, 0, -1))

    tree.fill(values, 3)

    print(tree)
