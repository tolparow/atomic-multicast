from utils.python23 import unicode_char, unicode_str


class TreeNodePrintableMixin:
    ANGLED_SYMBOL = unicode_char(9562)
    ANGLED_MERGED_SYMBOL = unicode_char(9568)
    ANGLED_T_SYMBOL = unicode_char(9574)
    VERTICAL_SYMBOL = unicode_char(9553)
    HORIZONTAL_SYMBOL = unicode_char(9552)

    OFFSET = 5

    def __str__(self):
        rows, vertical_lines = [], []
        self._print_node(rows=rows)

        for i in range(len(rows))[::-1]:
            v_lines = list(vertical_lines)
            for v_i in vertical_lines:
                if v_i < len(rows[i]):
                    if rows[i][v_i] == u' ':
                        rows[i][v_i] = TreeNodePrintableMixin.VERTICAL_SYMBOL
                    else:
                        v_lines.remove(v_i)
            vertical_lines = v_lines

            new_branch = rows[i].index(TreeNodePrintableMixin.ANGLED_SYMBOL)
            if i < len(rows) - 1 and \
                    len(rows[i + 1]) > new_branch and \
                    rows[i + 1][new_branch] in [TreeNodePrintableMixin.ANGLED_SYMBOL,
                                                TreeNodePrintableMixin.VERTICAL_SYMBOL]:
                rows[i][new_branch] = TreeNodePrintableMixin.ANGLED_MERGED_SYMBOL

            if i > 0 and rows[i - 1][new_branch] == TreeNodePrintableMixin.HORIZONTAL_SYMBOL:
                rows[i - 1][new_branch] = TreeNodePrintableMixin.ANGLED_T_SYMBOL

            vertical_lines.append(new_branch)

        return unicode_str(u'\n'.join([u''.join(row) for row in rows]))

    def _print_node(self, level=0, rows=[]):
        rows.append(
            list(
                u' ' * (level * TreeNodePrintableMixin.OFFSET) +
                TreeNodePrintableMixin.ANGLED_SYMBOL +
                TreeNodePrintableMixin.HORIZONTAL_SYMBOL * TreeNodePrintableMixin.OFFSET +
                u' ' + repr(self.value)
            )
        )

        for child in self.children:
            child._print_node(level + 1, rows)


class TreePrintableMixin:
    def __str__(self):
        return str(self.root)
