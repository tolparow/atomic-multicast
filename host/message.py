import socket
import time


class Message:
    """
    The Message has the following structure:
        # TODO maybe add some magic number at the beginning to indicate protocol (to filter out foreign messages)
        # but actually using of the specific port (8820) is pretty much filters out wrong messages
        1 byte  - type of message (in our case 1):
                  1 - 1st round message
                  2 - ACK of the first round
                  4 - 2nd round message
                  8 - ACK of the second round
                  # TODO add new types needed
        4 bytes - broadcaster IPv4 address (those who initialized broadcast)
        4 bytes - reportee: personal IPv4 to know where the message comming from:
                  parent IPv4 - if message (type 1 or 4)
                  child IPv4  - if ACK (type 2 or 8)
        8 bytes - Unix timestamp in nanoseconds (big-endian).
        # TODO - do we need to have any sort of checksum to reject definitely wrong messages?
        N bytes - message.
    """
    MSG_1ST_ROUND = 1
    MSG_1ST_ACK = 2
    MSG_2ND_ROUND = 4
    MSG_2ND_ACK = 8

    def __init__(self,
                 message_type: int or None,
                 broadcaster: str,
                 reportee: str,
                 timestamp: int or None,
                 message: bytes):
        """
        Create a Message object either from message with header, or from composing message with header parts.

        :param message_type: message type according to structure or None if message should be decomposed
        :param broadcaster: broadcaster IP. Ignored if message_type is None
        :param reportee: reportee IP. Ignored if message_type is None
        :param timestamp: timestamp or None to use current time. Ignored if message_type is None
        :param message: message body to be appended if message_type is not None, else message to extract header from
        """
        self.message_type = message_type
        self.broadcaster = broadcaster
        self.reportee = reportee
        self.timestamp = timestamp if timestamp is not None else int(time.time())
        self.message = message

        self.message_type_bytes = None
        self.broadcaster_bytes = None
        self.reportee_bytes = None
        self.timestamp_bytes = None

        self.bytes = None

        if self.message_type is None:
            self._decompose_message()
        else:
            self._compose_message()

    def get_updated_message(self, message_type: int) -> 'Message':
        """
        Update message_type and return new updated message.

        :param message_type: new message type
        :return new Message
        """
        return Message(message_type, self.broadcaster, self.reportee, self.timestamp, self.message)

    # def update_message(self, message_type: int, reportee: str) -> 'Message':
    #     """
    #     Update message_type and reportee.
    #
    #     :param message_type: newt message type
    #     :param reportee: new reportee
    #     :return self
    #     """
    #     self.message_type = message_type
    #     self.message_type_bytes = self.message_type.to_bytes(self.message_type, 'big')
    #
    #     self.reportee = reportee
    #     self.reportee_bytes = socket.inet_aton(self.reportee)
    #
    #     self._compose_bytes()
    #
    #     return self

    def _compose_message(self):
        """
        Compose the message from parts given.
        """
        self.message_type_bytes = self.message_type.to_bytes(1, 'big')
        self.broadcaster_bytes = socket.inet_aton(self.broadcaster)
        self.reportee_bytes = socket.inet_aton(self.reportee)
        self.timestamp_bytes = self.timestamp.to_bytes(8, 'big')

        self._compose_bytes()

    def _decompose_message(self):
        """
        Split the message onto parts and decode header to the convenient types.
        """
        if len(self.message) < 17:  # TODO change this number when header length changes
            raise ValueError('Message is too small to fit the header!')

        self.message_type, self.message_type_bytes = self.message[0], self.message[0:1]

        self.broadcaster_bytes = self.message[1:5]
        self.broadcaster = socket.inet_ntoa(self.broadcaster_bytes)

        self.reportee_bytes = self.message[5:9]
        self.reportee = socket.inet_ntoa(self.reportee_bytes)

        self.timestamp_bytes = self.message[9:17]
        self.timestamp = int.from_bytes(self.timestamp_bytes, 'big')

        self.bytes = self.message
        self.message = self.bytes[17:]

    def _compose_bytes(self):
        self.bytes = self.message_type_bytes + \
                     self.broadcaster_bytes + \
                     self.reportee_bytes + \
                     self.timestamp_bytes + \
                     self.message

    def __bytes__(self):
        return self.bytes
