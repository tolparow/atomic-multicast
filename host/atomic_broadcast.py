from host.message import Message
from host.utils.sending_server import SendingServer
from utils.trees import Trees


class ABNode(SendingServer):
    """
    Class for Atomic Broadcast Node.
    """


    def __init__(self, host: str, port: int = 8822,
                 topology_config_file: str = 'trees.json'):
        self.topology = Trees.read_matrix(topology_config_file)
        neighbors = self.topology[host][host]  # Children in the personal broadcast tree
        self.list_1st_round_messages = []
        self.list_2nd_round_messages = []
        self.time_to_live = 100
        super().__init__(host, port, neighbors, self._process_incoming_message)

    def _process_incoming_message(self, message_bytes: bytes):
        # TODO check if None means don't send the message
        if len(message_bytes) < 17:
            return None  # Indicate short (wrong) message

        message = Message(None, None, None, None, message_bytes)
        print ('message is received at', self.port, 'with type:' , message.message_type)
        if message.message_type == 1:
            return self._process_1st_round_message(message)
        elif message.message_type == 2:
            return self._process_1st_round_ack(message)
        elif message.message_type == 4:
            return self._process_2nd_round_message(message)
        elif message.message_type == 8:
            return self._process_2nd_round_ack(message)
        # TODO add new types needed

        return None  # Indicate message has a wrong header

    def broadcast_message(self, message: bytes = None) -> bool:
        """
        Broadcast message.

        :param message: message to be sent. If None is provided, the message is generated randomly.
        :return: True if message has been successfully broadcasted, False otherwise
        """

        message_bytes = self._prepend_1st_round_message_header(message)

        self.send_bulk_messages(message_bytes)

        # TODO wait for ACKs of the first round
        # TODO perform second round

        return True

    def _prepend_1st_round_message_header(self, message: bytes) -> bytes:
        """
        Creates the message for broadcasting by prepending a header (first round).

        :param message: message to be sent
        :return: pepended with a proper header message
        """

        return Message(
            Message.MSG_1ST_ROUND,
            self.host,
            self.host,
            None,
            message
        ).bytes
    def initiate_messaging(self, message: Message):
        children = self.topology[self.host][self.host]
        children_to_add = children.copy()
        self.list_1st_round_messages.append([message, children_to_add, self.time_to_live])
        message_to_children = message.get_updated_message(Message.MSG_1ST_ROUND)
        self.send_bulk_messages(message_to_children.bytes, children)

    def _process_1st_round_message(self, message: Message):
        """
        Process first round message:
            1. Pass to children
            2. TODO ...
            3. PROFIT!
        :param message: message received
        :return: response to send to parent (ACK)
        """

        # Propagate to children
        children = self.topology[message.broadcaster][self.host]
        children_to_add = children.copy()
        #add message to list and wait for ack
        #if the message is received at the leaf node, initiate 1st round ack messages by sending to itself
        if len(children) == 0:
            print(self.port, 'sending ack back to reportee',message.reportee, ' from leaf node',self.port )
            self.list_2nd_round_messages.append([message, [], self.time_to_live])
            upper_node = message.reportee
            message_to_upper_node = message.get_updated_message(Message.MSG_1ST_ACK)
            message_to_upper_node.reportee = self.host
            message_to_upper_node = message_to_upper_node.get_updated_message(Message.MSG_1ST_ACK)
            self.send_bulk_messages(message_to_upper_node.bytes, [upper_node])
        #if there are childrens, send message to children and wait for ack
        else:
            self.list_1st_round_messages.append([message, children_to_add, self.time_to_live])
            message_to_children = message.get_updated_message(Message.MSG_1ST_ROUND)
            message_to_children.reportee = self.host
            message_to_children = message_to_children.get_updated_message(Message.MSG_1ST_ROUND)
            self.send_bulk_messages(message_to_children.bytes, children)
            #wait for acknowledgments
        # Return response to parent
        #return message.get_updated_message(Message.MSG_1ST_ACK)
        pass

    def _process_1st_round_ack(self, message: Message):
        print('1st round ack is handling at',self.port, 'which came from', message.reportee)
        #propogate ack message to the sender

        #check message list stored
        for message_triple in self.list_1st_round_messages:
            message_at_list = message_triple[0]
            #and find the message registered with unique broadcaster and timestamp combination
            if ((message_at_list.broadcaster == message.broadcaster) and (message_at_list.timestamp == message.timestamp)) and (message.reportee in message_triple[1]):
                message_triple[1].remove(message.reportee)

                if len(message_triple[1]) == 0:
                    children = self.topology[message.broadcaster][self.host]
                    children_to_add = children.copy()

                    #if the broadcaster received the message send second phase message to itself()
                    if message.broadcaster == self.host:
                        print (self.port, 'broadcaster received')
                        children = self.topology[message.broadcaster][self.host]
                        children_to_add = children.copy()
                        self.list_2nd_round_messages.append([message_at_list, children_to_add, self.time_to_live])
                        message.reportee = self.host
                        message_to_children = message.get_updated_message(Message.MSG_2ND_ROUND)
                        self.send_bulk_messages(message_to_children.bytes, children)
                    #if any node except broadcaster received the message, pass the ACK message to the reportee
                    #send back ack to reportee for the message
                    else:
                        print (self.port, 'seding ack back to reportee',message_at_list.reportee)
                        self.list_2nd_round_messages.append([message_at_list, [], self.time_to_live])
                        upper_node = message_at_list.reportee
                        #message.reportee = message_at_list.reportee
                        message.reportee = self.host
                        message_to_upper_node = message.get_updated_message(Message.MSG_1ST_ACK)
                        self.send_bulk_messages(message_to_upper_node.bytes, [upper_node])


                    # Remove this message from the list.
                    self.list_1st_round_messages.remove(message_triple)
        pass

    def _process_2nd_round_message(self, message: Message):
        # check message list stored
        for message_triple in self.list_2nd_round_messages:
            message_at_list = message_triple[0]
            # and find the message registered with unique broadcaster and timestamp combination
            if ((message_at_list.broadcaster == message.broadcaster) and (message_at_list.timestamp == message.timestamp)):
                children = self.topology[message.broadcaster][self.host]
                children_to_add = children.copy()
                message.reportee = self.host
                message_to_children = message.get_updated_message(Message.MSG_2ND_ROUND)
                self.send_bulk_messages(message_to_children.bytes, children_to_add)
                self.list_2nd_round_messages.remove(message_triple)
                print ('message ACK is done at node',self.host)
        pass

    def _process_2nd_round_ack(self, message: Message):
        pass
