import socket
import threading
from random import choice, random
from time import sleep

from host.utils.threaded_server import ThreadedServer, echo


class SendingServer(ThreadedServer):

    def listen(self, bg=True):
        if bg:
            threading.Thread(target=super().listen).start()
        else:
            super().listen()

    def send_message(self, host=None, port=None, message: bytes = None, wait_reply=False):
        host = host if host is not None else choice(self.neighbors)
        port = port if port is not None else ThreadedServer.PORT
        message = message if message is not None else b'Hello!'

        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.sendto(message, (host, port))
        data = s.recv(4096) if wait_reply else None

        s.close()

        return data

    def send_bulk_messages(self, message: bytes, hosts: list = None, port: int = None):
        """
        Send messages to many recipients. Do not wait for the response.
        :param message: message to be sent
        :param hosts: hosts to receive messages. If None is provided, use self.neighbors
        :param port: port of hosts to receive message. If None is provided, use self.port
        :return:
        """
        hosts = hosts if hosts is not None else self.neighbors
        port = port if port is not None else self.port

        for host in hosts:
            self.send_message(host, port, message)

    def send_random_messages(self, host=None, port=None, message=None, max_interval=1, min_interval=0.1):
        """
        Sends random messages to one host. For testing only.
        :param host:
        :param port:
        :param message:
        :param max_interval:
        :param min_interval:
        :return:
        """
        while True:
            sleep(random() * (max_interval - min_interval) + min_interval)
            self.send_message(host, port, message)

    def send_single_message(self, host=None, port=None, message=None):
        """
        Sends random messages to one host. For testing only.
        :param host:
        :param port:
        :param message:
        :param max_interval:
        :param min_interval:
        :return:
        """
        self.send_message(host, port, message)
