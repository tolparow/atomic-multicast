#!/usr/bin/python3

import socket
import threading


class ThreadedServer(object):
    PORT = 8822

    def __init__(self, host: str, port: int,
                 neighbors: list,
                 server_callback_function,
                 backlog: int = 5, client_timeout: int = 60, size: int = 4096):
        self.host = host
        self.port = port

        self.backlog = backlog
        self.client_timeout = client_timeout
        self.size = size

        self.neighbors = neighbors

        self.server_callback_function = server_callback_function

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def listen(self):
        print(self.host,self.port)
        self.sock.bind((self.host, self.port))

        while True:
            data, address = self.sock.recvfrom(self.size)

            threading.Thread(target=self._thread_listen_client, args=(data, address)).start()

    def _thread_listen_client(self, data, address):
        if data:
            response = self.server_callback_function(data)
            self.sock.sendto(response, address)


def echo(request: bytes) -> bytes:
    print(request, flush=True)
    return request


if __name__ == '__main__':
    server = ThreadedServer('', 8822, [], echo)

    server.listen()
