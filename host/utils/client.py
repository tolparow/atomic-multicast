#!/usr/bin/python3

from socket import *

s = socket(AF_INET, SOCK_DGRAM)
s.sendto(b'Hello , world', ('127.0.0.1', 80))
data = s.recv(4096)
print(data)

s.close()